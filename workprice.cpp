#include "workprice.h"
#include "ui_workprice.h"

workPrice::workPrice(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::workPrice)
{
    ui->setupUi(this);
    model = new QSqlRelationalTableModel(this);
    model->setTable("work_price");
    model->setRelation(model->fieldIndex("emp"), QSqlRelation("person", "id", "name"));
    model->select();

    //model->removeColumn(model->fieldIndex("id"));
    model->setHeaderData(0, Qt::Horizontal, "编号");
    model->setHeaderData(1,  Qt::Horizontal, "姓名");
    model->setHeaderData(model->fieldIndex("work_price"), Qt::Horizontal, "基本时薪");
    model->setHeaderData(model->fieldIndex("over_price"), Qt::Horizontal, "加班时薪");
    model->setHeaderData(model->fieldIndex("leave_price"), Qt::Horizontal, "请假扣费");
    model->setHeaderData(model->fieldIndex("errand_price"), Qt::Horizontal, "出差补贴");
    model->setHeaderData(model->fieldIndex("late_price"), Qt::Horizontal, "迟到扣费");
    model->setHeaderData(model->fieldIndex("early_price"), Qt::Horizontal, "早退扣费");
    model->setHeaderData(model->fieldIndex("absent_price"), Qt::Horizontal, "缺席扣费");

    ui->treeView->setModel(model);
    ui->treeView->setItemDelegate(new QSqlRelationalDelegate(ui->treeView));
    setWindowTitle("工资设定");
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(submitChange()));
}

workPrice::~workPrice()
{
    delete ui;
}

bool workPrice::submitChange()
{
    if (model->submitAll())
        ui->label->setText("<p style='color:green;'>修改成功！</p>");
    else
    {
        ui->label->setText("<p style='color:red'>修改失败</p>");
        return false;
    }
    return true;
}
