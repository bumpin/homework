#ifndef ENDTABLE_H
#define ENDTABLE_H

#include <QDialog>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlRelationalTableModel>
#include <QtSql/QSqlRelationalDelegate>
#include <QDebug>

namespace Ui {
    class endTable;
}

class endTable : public QDialog
{
    Q_OBJECT

public:
    explicit endTable(QWidget *parent = 0);
    void updateSalary();
    ~endTable();

private:
    Ui::endTable *ui;
    QSqlRelationalTableModel *model;
    QSqlDatabase db;
    struct endTableItem
    {
        int basic;
        int over;
        int leave;
        int errand;
        int late;
        int early;
        int absent;
        int total;
    }item;
};

#endif // ENDTABLE_H
