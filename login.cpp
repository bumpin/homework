#include "login.h"
#include "ui_login.h"
#include "mainwindow.h"
#include <QtSql/QSqlDatabase>
#include <QDebug>

login::login(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::login)
{
    ui->setupUi(this);
    connect(ui->pushButton, SIGNAL(clicked()), qApp, SLOT(quit()));
    setWindowTitle("用户登录");

    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(checkUser()));
    connect(ui->lineEdit, SIGNAL(returnPressed()), this, SLOT(checkUser()));
    connect(ui->lineEdit_2, SIGNAL(returnPressed()), this, SLOT(checkUser()));

}

login::~login()
{
    delete ui;
}

void login::checkUser()
{
    //添加MySQL数据库
    db = QSqlDatabase::addDatabase("QMYSQL");
    db.setHostName("localhost");
    db.setDatabaseName("salary");
    db.setUserName(ui->lineEdit->text());
    db.setPassword(ui->lineEdit_2->text());

    if (db.open())
    {
        this->close();
        MainWindow *m = new MainWindow(this);
        m->show();
    }
    else
    {
        qDebug() << "failure";
        ui->errorLabel->setText("<p style='color:red;'>链接失败，请输入正确的用户名和密码并配置好数据库</p>");
    }
}
