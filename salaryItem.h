#ifndef SALARYITEM_H
#define SALARYITEM_H

namespace DataStruct {
    struct endTableItem;
}

struct endTableItem
{
    int basic;
    int over;
    int leave;
    int errand;
    int late;
    int early;
    int absent;
    int total;
};

#endif // SALARYITEM_H
