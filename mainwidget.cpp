#include "mainwidget.h"
#include "ui_mainwidget.h"
#include "mainwindow.h"
#include <QModelIndex>
#include <QSqlRecord>
#include <QSqlError>
#include <QString>

mainWidget::mainWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::mainWidget)
{
    newEmpList = new vector<int>();
    ui->setupUi(this);
    model = new QSqlRelationalTableModel();
    model->setTable("person");
    model->setRelation(2, QSqlRelation("sex", "id", "f_or_m"));
    model->setRelation(6, QSqlRelation("department", "id", "name"));
    model->setHeaderData(0, Qt::Horizontal, "员工号");
    model->setHeaderData(1, Qt::Horizontal, "姓名");
    model->setHeaderData(2, Qt::Horizontal, "性别");
    model->setHeaderData(3, Qt::Horizontal, "密码");
    model->setHeaderData(4, Qt::Horizontal, "权限");
    model->setHeaderData(5, Qt::Horizontal, "生日");
    model->setHeaderData(6, Qt::Horizontal, "部门");
    model->setHeaderData(7, Qt::Horizontal, "职务");
    model->setHeaderData(8, Qt::Horizontal, "其他");

    model->select();

    ui->treeView->setModel(model);
    ui->treeView->setItemDelegate(new QSqlRelationalDelegate(ui->treeView));

    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(submitChange()));
    connect(ui->treeView, SIGNAL(clicked(QModelIndex)), parent, SLOT(rowClicked(QModelIndex)));

    //resize column
    ui->treeView->resizeColumnToContents(0);
    ui->treeView->resizeColumnToContents(4);
}

mainWidget::~mainWidget()
{
    delete ui;
}

bool mainWidget::submitChange()
{
    if (model->submitAll())
    {
        ui->label->setText("<p style='color:green;'>修改成功！</p>");
    }
    else
    {
        ui->label->setText("<p style='color:red'>修改失败</p>");
        return false;
    }

    vector<int>::iterator it = newEmpList->begin();
    for(; it != newEmpList->end(); it++)
    {
        QSqlQuery query;
        query.prepare("insert into work_price(emp) values(:emp)");
        query.bindValue(":emp", *it);
        query.exec();

        query.exec(QString("insert into attendance_state(person) values(%1)").arg(*it));

        query.prepare("insert into salary(person) values(:emp)");
        query.bindValue(":emp", *it);
        query.exec();
    }
    return true;
}

void mainWidget::deleteEmp()
{
    model->submitAll();
    int rowNum =  ui->treeView->currentIndex().row();
    int empNumber;
    QSqlRecord record;
    record = model->record(rowNum);
    empNumber =  record.value("id").toInt();
    qDebug() << empNumber;

    QSqlQuery Qr;
    qDebug() << Qr.exec(QString("delete from attendance_state  where person=%1").arg(empNumber));
    qDebug() << Qr.lastError().text();

    qDebug() << Qr.exec(QString("delete from work_price Where  emp=%1").arg(empNumber));
    qDebug() << Qr.lastError().text();

    qDebug() << Qr.exec(QString("delete from salary Where  person=%1").arg(empNumber));
    qDebug() << Qr.lastError().text();

    qDebug() << model->removeRow(rowNum);
    qDebug() << model->lastError().text();
}

void mainWidget::addEmp()
{
    int row = 0;
    row = model->rowCount() + 1;
    model->insertRow(0);
    model->setData(model->index(0, 0), row);
    newEmpList->push_back(row);
}
