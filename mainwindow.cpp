#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QSqlRelationalDelegate>
#include "mainwidget.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    tips = new QLabel();
    tips->setText("双击可以修改员工信息");
    ui->setupUi(this);
    setWindowTitle("工资管理系统——BY老唐");
    mainform = new mainWidget(this);
    this->setCentralWidget(mainform);

    connect(ui->aboutAction, SIGNAL(triggered()), this, SLOT(aboutSMS()));
    connect(ui->exitAction,  SIGNAL(triggered()), this, SLOT(exitComfirm()));
    connect(ui->delAction, SIGNAL(triggered()), mainform, SLOT(deleteEmp()));
    connect(ui->addAction, SIGNAL(triggered()), mainform, SLOT(addEmp()));
    connect(ui->attAction, SIGNAL(triggered()), this, SLOT(showAtt()));
    connect(ui->rapAction, SIGNAL(triggered()), this, SLOT(showPrice()));
    connect(ui->outAction, SIGNAL(triggered()), this, SLOT(showEnd()));
    connect(ui->departAction, SIGNAL(triggered()), this, SLOT(showDepart()));
    ui->statusBar->addWidget(tips);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::aboutSMS()
{
    QMessageBox::about(
                this,
                "关于工资管理软件",
                "本程序是老唐(<a href='www.yucoat.com'>www.yucoat.com</a>)课程设计的作品。\n\n"
                "仅限于学习交流，禁止在未经作者允许的情况下用于任何商业用途！\n"
                "本作者会竭尽全力修复软件中的BUG，但不对该软件作任何担保！\n\n"
                "你可以发送邮件至 <a href='mailto:thlgood@yucoat.com'>thlgood@yucoat.com</a> 与作者取得联系。\n\n"
                );
}

void MainWindow::exitComfirm()
{
    int r = QMessageBox::warning(this,
                                 "确定确认",
                                 "您确定要退出本程序吗？没有保存的数据可能丢失哦！",
                                 QMessageBox::Yes|QMessageBox::No|QMessageBox::Cancel);
    if(r == QMessageBox::Yes)
        qApp->quit();
}

void MainWindow::rowClicked(QModelIndex index)
{
    ui->attAction->setEnabled(true);
    ui->empInfoAction->setEnabled(true);
    ui->delAction->setEnabled(true);
    ui->rapAction->setEnabled(true);
}

void MainWindow::showAtt()
{
    Attendance *att = new Attendance(this);
    att->show();

}

void MainWindow::showPrice()
{
    workPrice *w = new workPrice(this);
    w->show();
}

void MainWindow::showEnd()
{
    endTable *w = new endTable(this);
    w->show();
}

void MainWindow::showDepart()
{
    dpDialog *w = new dpDialog(this);
    w->show();
}

