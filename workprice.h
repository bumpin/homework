#ifndef WORKPRICE_H
#define WORKPRICE_H

#include <QDialog>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlRelationalTableModel>
#include <QtSql/QSqlRelationalDelegate>
#include <QDebug>

namespace Ui {
    class workPrice;
}

class workPrice : public QDialog
{
    Q_OBJECT

public:
    explicit workPrice(QWidget *parent = 0);
    ~workPrice();

private:
    Ui::workPrice *ui;
    QSqlDatabase db;
    QSqlRelationalTableModel *model;
private slots:
    bool submitChange();
};

#endif // WORKPRICE_H
