#ifndef DPDIALOG_H
#define DPDIALOG_H

#include <QDialog>
#include <QDebug>
#include <QSqlRelationalDelegate>
#include <QtSql/QSqlRelationalTableModel>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QSqlTableModel>
#include <QSqlRelation>

namespace Ui {
    class dpDialog;
}

class dpDialog : public QDialog
{
    Q_OBJECT

public:
    explicit dpDialog(QWidget *parent = 0);
    ~dpDialog();

private:
    Ui::dpDialog *ui;
    QSqlDatabase db;
    QSqlRelationalTableModel *model;
    int currRow;
private slots:
    void insertLine();
    void submit();
    void picLine(QModelIndex);
    void deleteLine();
};

#endif // DPDIALOG_H
