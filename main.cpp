#include <QtGui/QApplication>
#include "mainwindow.h"
#include "login.h"
#include "attendance.h"
#include <QTextCodec>
#include <QMainWindow>
#include "workprice.h"
#include "endtable.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QTextCodec *codec = QTextCodec::codecForName("system");
    QTextCodec::setCodecForTr(codec);
    QTextCodec::setCodecForLocale(codec);
    QTextCodec::setCodecForCStrings(codec);

    login *loginForm = new login();
    loginForm->show();

    return a.exec();
}
