#-------------------------------------------------
#
# Project created by QtCreator 2012-12-25T14:38:33
#
#-------------------------------------------------

QT       += core gui

TARGET = SMS
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    login.cpp \
    mainwidget.cpp \
    attendance.cpp \
    workprice.cpp \
    endtable.cpp \
    dpdialog.cpp

HEADERS  += mainwindow.h \
    login.h \
    mainwidget.h \
    attendance.h \
    workprice.h \
    endtable.h \
    salaryItem.h \
    dpdialog.h

FORMS    += mainwindow.ui \
    login.ui \
    mainwidget.ui \
    attendance.ui \
    workprice.ui \
    endtable.ui \
    dpdialog.ui

RESOURCES += \
    icons/source.qrc

QT += sql
