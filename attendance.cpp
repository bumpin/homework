#include "attendance.h"
#include "ui_attendance.h"
#include <QDebug>

Attendance::Attendance(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Attendance)
{
    ui->setupUi(this);
    model = new QSqlRelationalTableModel(this);
    model->setTable("attendance_state");
    model->setRelation(2, QSqlRelation("person", "id", "name"));
    model->select();

    model->setHeaderData(model->fieldIndex("id"), Qt::Horizontal, "编号");
    model->setHeaderData(model->fieldIndex("name"), Qt::Horizontal, "姓名");
    model->setHeaderData(model->fieldIndex("year_month"), Qt::Horizontal, "年月");
    model->setHeaderData(model->fieldIndex("work_hour"), Qt::Horizontal, "工作小时");
    model->setHeaderData(model->fieldIndex("over_hour"), Qt::Horizontal, "加班小时");
    model->setHeaderData(model->fieldIndex("leave_hday"), Qt::Horizontal, "请假时间");
    model->setHeaderData(model->fieldIndex("errand_hday"), Qt::Horizontal, "出差时间");
    model->setHeaderData(model->fieldIndex("late_times"), Qt::Horizontal, "加班小时");
    model->setHeaderData(model->fieldIndex("early_times"), Qt::Horizontal, "早退次数");
    model->setHeaderData(model->fieldIndex("absent_times"), Qt::Horizontal, "缺席次数");

    ui->treeView->setModel(model);
    ui->treeView->setItemDelegate(new QSqlRelationalDelegate(ui->treeView));

    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(submitChange()));
    setWindowTitle("月度考勤表");
}

Attendance::~Attendance()
{
    delete ui;
}

bool Attendance::submitChange()
{
    if (model->submitAll())
        ui->label->setText("<p style='color:green;'>修改成功！</p>");
    else
    {
        ui->label->setText("<p style='color:red'>修改失败</p>");
        return false;
    }
    return true;
}
