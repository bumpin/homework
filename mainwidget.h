#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QDebug>
#include <QAbstractItemView>
#include <QtSql/QSqlRelationalDelegate>
#include <QtSql/QSqlDatabase>
#include <QSize>
#include "QSqlQuery"
#include <vector>
using namespace std;

namespace Ui {
    class mainWidget;
}

class mainWidget : public QWidget
{
    Q_OBJECT

public:
    explicit mainWidget(QWidget *parent = 0);
    void connectDataBase();
    ~mainWidget();

private:
    //QSqlDatabase *db;
    Ui::mainWidget *ui;
    QSqlRelationalTableModel *model;
    vector<int> *newEmpList;

private slots:
    bool submitChange();
    void deleteEmp();
    void addEmp();
};
#endif // MAINWIDGET_H
