#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QLabel>
#include <QMessageBox>
#include <QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QSqlTableModel>
#include <QSqlRelationalTableModel>
#include <QModelIndex>
#include "mainwidget.h"
#include "attendance.h"
#include "workprice.h"
#include "endtable.h"
#include "dpdialog.h"

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    QSqlDatabase *db;
    QSqlRelationalTableModel *model;
    QLabel *tips;
    mainWidget *mainform;
private slots:
    void aboutSMS();
    void exitComfirm();
    void rowClicked(QModelIndex);
    void showAtt();
    void showPrice();
    void showEnd();
    void showDepart();
};

#endif // MAINWINDOW_H
