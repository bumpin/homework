#include "dpdialog.h"
#include "ui_dpdialog.h"

dpDialog::dpDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::dpDialog)
{
    ui->setupUi(this);
    model = new QSqlRelationalTableModel(this);
    ui->treeView->setModel(model);
    ui->pushButton_3->setEnabled(false);

    model->setTable("department");
    model->setHeaderData(0, Qt::Horizontal, "部门编号");
    model->setHeaderData(1, Qt::Horizontal, "部门名称");
    model->setHeaderData(2, Qt::Horizontal, "部门经理");
    model->setHeaderData(3, Qt::Horizontal, "部门简介");
    //model->setRelation(2, QSqlRelation("person", "id", "name"));

    model->select();
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(insertLine()));
    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(submit()));
    connect(ui->treeView, SIGNAL(clicked(QModelIndex)), this, SLOT(picLine(QModelIndex)));
    connect(ui->pushButton_3, SIGNAL(clicked()), this, SLOT(deleteLine()));

    ui->treeView->setItemDelegate(new QSqlRelationalDelegate(ui->treeView));
    setWindowTitle("部门管理");
}

dpDialog::~dpDialog()
{
    delete ui;
}

void dpDialog::insertLine()
{
    int row = 0;
    model->insertRow(row);
    model->setData(model->index(row, 0), 0);
    model->setData(model->index(row, 1), "");
    model->setData(model->index(row, 2), "");
    model->setData(model->index(row, 3), "");
}

void dpDialog::submit()
{
    if (!model->submitAll())
        qDebug() << "插入失败";
    else
        qDebug() << "插入成功";
    //this->close();
    this->hide();
}

void dpDialog::picLine(QModelIndex index)
{
    ui->pushButton_3->setEnabled(true);
    currRow =  index.row();
}

void dpDialog::deleteLine()
{
    qDebug() << currRow;
    if (model->removeRow(currRow))
        qDebug() << "success remove";
    else
        qDebug() << "false remove";
}
