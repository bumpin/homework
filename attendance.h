#ifndef ATTENDANCE_H
#define ATTENDANCE_H

#include <QDialog>
#include <QtSql/QSqlRelationalTableModel>
#include <QtSql/QSqlRelationalDelegate>
namespace Ui {
    class Attendance;
}

class Attendance : public QDialog
{
    Q_OBJECT

public:
    explicit Attendance(QWidget *parent = 0);
    ~Attendance();

private:
    Ui::Attendance *ui;
    QSqlRelationalTableModel *model;
    QSqlDatabase db;
private slots:
    bool submitChange();
};


#endif // ATTENDANCE_H
