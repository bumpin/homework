#include "endtable.h"
#include "ui_endtable.h"
#include <QSqlRecord>
//using namespace Ui;
endTable::endTable(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::endTable)
{
    updateSalary();
    ui->setupUi(this);
    model = new QSqlRelationalTableModel(this);
    model->setTable("salary");

    model->setRelation(2, QSqlRelation("person", "id", "name"));
    model->setHeaderData(model->fieldIndex("id"), Qt::Horizontal, "编号");
    model->setHeaderData(model->fieldIndex("yearmonth"), Qt::Horizontal, "年月");
    model->setHeaderData(model->fieldIndex("person"), Qt::Horizontal, "姓名");
    model->setHeaderData(model->fieldIndex("basic"), Qt::Horizontal, "基本工资");
    model->setHeaderData(model->fieldIndex("over"), Qt::Horizontal, "加班工资");
    model->setHeaderData(model->fieldIndex("leave"), Qt::Horizontal, "请假扣费");
    model->setHeaderData(model->fieldIndex("errand"), Qt::Horizontal, "出差补贴");
    model->setHeaderData(model->fieldIndex("late"), Qt::Horizontal, "迟到罚款");
    model->setHeaderData(model->fieldIndex("early"), Qt::Horizontal, "早退罚款");
    model->setHeaderData(model->fieldIndex("absent"), Qt::Horizontal, "缺席罚款");
    model->setHeaderData(model->fieldIndex("total"), Qt::Horizontal, "应发工资");
    model->select();

    ui->treeView->setModel(model);
    ui->treeView->setItemDelegate(new QSqlRelationalDelegate(ui->treeView));
    setWindowTitle("工资报表");
}

endTable::~endTable()
{
    delete ui;
}

void endTable::updateSalary()
{
    int personID;

    QSqlTableModel *personModel = new QSqlTableModel();
    QSqlTableModel *attModel  = new QSqlTableModel();;
    QSqlTableModel *priceModel  = new QSqlTableModel();
    QSqlTableModel *salaryModel = new QSqlTableModel();

    personModel->setTable("person");
    attModel->setTable("attendance_state");
    priceModel->setTable("work_price");
    salaryModel->setTable("salary");

    personModel->select();
    //attModel->select();
    //priceModel->select();
    //salaryModel->select();

    for(int i = 0; i < personModel->rowCount(); i++)
    {
        QSqlRecord record = personModel->record(i);
        QSqlRecord salaryRecord;
        personID = record.value("id").toInt();
        attModel->setFilter(QString("person=%1").arg(personID));
        attModel->select();

        priceModel->setFilter(QString("emp=%1").arg(personID));
        priceModel->select();

        item.basic = attModel->record(0).value("work_hour").toInt() * priceModel->record(0).value("work_price").toInt();
        item.over = attModel->record(0).value("over_hour").toInt() * priceModel->record(0).value("over_price").toInt();
        item.leave = attModel->record(0).value("leave_hday").toInt() * priceModel->record(0).value("leave_price").toInt();
        item.errand = attModel->record(0).value("errand_hday").toInt() * priceModel->record(0).value("errand_price").toInt();
        item.late = attModel->record(0).value("late_times").toInt() * priceModel->record(0).value("late_price").toInt();
        item.early = attModel->record(0).value("early_times").toInt() * priceModel->record(0).value("early_price").toInt();
        item.absent = attModel->record(0).value("absent_times").toInt() * priceModel->record(0).value("absent_price").toInt();
        item.total = item.basic + item.over + item.errand - item.late -item.leave - item.early - item.absent;

        salaryModel->setFilter(QString("person=%1").arg(personID));
        salaryModel->select();
        salaryRecord = salaryModel->record(0);

        salaryModel->setData(salaryModel->index(0, salaryModel->fieldIndex("basic")), item.basic);
        salaryModel->setData(salaryModel->index(0, salaryModel->fieldIndex("over")), item.over);
        salaryModel->setData(salaryModel->index(0, salaryModel->fieldIndex("leave")), item.leave);
        salaryModel->setData(salaryModel->index(0, salaryModel->fieldIndex("errand")), item.errand);
        salaryModel->setData(salaryModel->index(0, salaryModel->fieldIndex("late")), item.late);
        salaryModel->setData(salaryModel->index(0, salaryModel->fieldIndex("early")), item.early);
        salaryModel->setData(salaryModel->index(0, salaryModel->fieldIndex("absent")), item.absent);
        salaryModel->setData(salaryModel->index(0, salaryModel->fieldIndex("total")), item.total);
        salaryModel->submitAll();
    }
}
